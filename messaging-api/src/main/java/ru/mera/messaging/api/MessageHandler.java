package ru.mera.messaging.api;

/**
 * Created by aivaliev on 14.12.2016.
 */
public interface MessageHandler {

    void handleMessage(Message message);
}
