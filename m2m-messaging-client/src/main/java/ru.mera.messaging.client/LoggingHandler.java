package ru.mera.messaging.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.mera.messaging.api.Message;
import ru.mera.messaging.api.MessageHandler;
import ru.mera.messaging.kafka.Consumer;

/**
 * Created by aivaliev on 15.12.2016.
 */
public class LoggingHandler implements MessageHandler {

    Logger logger = LoggerFactory.getLogger(Consumer.class.getName());

    @Override
    public void handleMessage(Message record) {
        logger.info(String.format(
                "KafkaMessage has been get key = %s, value = %s",
                record.getKey(),
                record.getValue()));
    }
}
