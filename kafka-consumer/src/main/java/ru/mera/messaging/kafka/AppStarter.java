package ru.mera.messaging.kafka;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by LocalAdmin on 07.12.2016.
 */
public class AppStarter implements BundleActivator {

    Logger logger = LoggerFactory.getLogger(AppStarter.class);

    Consumer consumer;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
//        logger.info("STARTING Kafka-consumer bundle");
//        consumer = new Consumer();
//        Thread thread = new Thread(consumer);
//        thread.start();
//        logger.info("ACTIVE Kafka-consumer bundle");

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        logger.info("STOPPING Kafka-consumer bundle");
        consumer.stop();
        logger.info("STOPPED Kafka-consumer bundle");
    }
}
