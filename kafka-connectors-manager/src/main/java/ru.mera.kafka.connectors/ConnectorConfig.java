package ru.mera.kafka.connectors;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by LocalAdmin on 19.12.2016.
 */

public class ConnectorConfig implements Serializable {

    private String name;
    private Map<String, String> config;

    public void setName(String name) {
        this.name = name;
    }

    public void setConfig(Map<String, String> config) {
        this.config = config;
    }

    public String getName() {
        return name;
    }

    public Map<String, String> getConfig() {
        return config;
    }
}
