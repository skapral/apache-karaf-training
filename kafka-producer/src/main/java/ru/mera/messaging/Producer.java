package ru.mera.messaging;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;


import java.time.Instant;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by LocalAdmin on 05.12.2016.
 */
public class Producer implements AutoCloseable {

    final static String DEFAULT_TOPIC = "test";

    private KafkaProducer<String, String> kafkaProducer;


    public Producer() {

        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        kafkaProducer = new KafkaProducer<String, String>(configProperties);

    }


    public void send(String topic, String value) {
        ProducerRecord record = new ProducerRecord(topic, value);
        kafkaProducer.send(record);
    }

    public void send(String value) {
        ProducerRecord record = new ProducerRecord(DEFAULT_TOPIC, Instant.now().toString(), value);
        kafkaProducer.send(record);
    }


    @Override
    public void close() throws Exception {
        kafkaProducer.close();
    }
}
